//handle deposit button
document
  .getElementById("deposit-button")
  .addEventListener("click", function () {
    if (isNaN(parseFloat(document.getElementById("deposit-amount").value))) {
      alert("please enter valid input");
      document.getElementById("deposit-amount").value = "";
    } else {
      //get the depodit amount
      const depositInputText = document.getElementById("deposit-amount");
      let currentDeposit = depositInputText.value;
      if (depositInputText.value == "") {
        currentDeposit = 0;
        //   console.log(currentDeposit);
      }
      // console.log(currentDeposit);
      const previousDepositText = document.getElementById("deposit-total");
      let previousDepositAmount = previousDepositText.innerText;
      const totalDepositAmount =
        Number.parseFloat(currentDeposit) +
        Number.parseFloat(previousDepositAmount);
      previousDepositText.innerText = totalDepositAmount;

      //update balance
      const balanceTotal = document.getElementById("balance-total");
      const totalBalanceText = balanceTotal.innerText;
      balanceTotal.innerText =
        parseFloat(totalBalanceText) + parseFloat(currentDeposit);
      // console.log(totalBalanceText);
      // clear deposit amount
      depositInputText.value = "";
    }
  });

//handle widthdraw button
document
  .getElementById("widthdraw-button")
  .addEventListener("click", function () {
    if (isNaN(parseFloat(document.getElementById("widthdraw-amount").value))) {
      alert("please enter valid input");
      document.getElementById("widthdraw-amount").value = "";
    } else {
      //get the depodit amount
      const widthdrawInputText = document.getElementById("widthdraw-amount");
      let currentwidthdraw = widthdrawInputText.value;
      if (widthdrawInputText.value == "") {
        currentwidthdraw = 0;
        //   console.log(currentwidthdraw);
      }
      // console.log(currentwidthdraw);

      const balanceTotal = document.getElementById("balance-total");
      if (parseFloat(currentwidthdraw) > parseFloat(balanceTotal.innerText)) {
        alert("Baper taka kom porese");
      } else {
        //update balance
        const previouswidthdrawText =
          document.getElementById("widthdraw-total");
        let previouswidthdrawAmount = previouswidthdrawText.innerText;
        const totalwidthdrawAmount =
          Number.parseFloat(currentwidthdraw) +
          Number.parseFloat(previouswidthdrawAmount);
        previouswidthdrawText.innerText = totalwidthdrawAmount;
        const totalBalanceText = balanceTotal.innerText;
        balanceTotal.innerText =
          parseFloat(totalBalanceText) - parseFloat(currentwidthdraw);
      }

      // console.log(totalBalanceText);
      // clear widthdraw amount
      widthdrawInputText.value = "";
    }
  });
